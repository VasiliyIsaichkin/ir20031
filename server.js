const http = require('http');
const Koa = require('koa');
const koa = new Koa();
const IO = require('socket.io');
const {createReadStream} = require('fs');
const koaStatic = require('koa-static');

koa.use(koaStatic('./src/assets'));
koa.use(koaStatic('./src/static'));
koa.use(koaStatic('./dist'));

const server = http.createServer(koa.callback());
const io = new IO(server);

koa.use(ctx => {
	ctx.type = 'html';
	ctx.body = createReadStream('./dist/index.html');
});

const state = {};

io.on('connection', function (socket) {
	const id = socket.id;

	socket.on('updateFromClient', (info) => {
		state[id] = {id, dir: info.dir, type: info.type};
		console.log(state[id])
		io.emit('updateFromServer', state[id]);
	});

	socket.on('start', (type, ic) => {
		socket.emit('state', state);
	});

	socket.on('disconnect', () => {
		io.emit('delete', {id});

		delete state[id];
	});
});


server.listen(3000);
