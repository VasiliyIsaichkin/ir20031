# Project IR-20031

## Project setup
```
npm install
```

### Compiles and minifies for production
```
npm run build
```

### Run server
```
npm run server
```
