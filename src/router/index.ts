import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Num2Num.vue';

Vue.use(VueRouter);

const routes = [
	{
		path     : '/test1',
		name     : 'Bin2Dec',
		component: Home
	}, {
		path     : '/test2',
		name     : 'Game',
		component: () => import('../views/Game.vue')
	}, {
		path    : '*',
		redirect: '/test1'
	}
];

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
});

export default router;
