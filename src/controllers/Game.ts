import Phaser from 'phaser';
import {iterate} from 'osmium-tools';

type Chars = CharTourtle | CharShark | CharSeaStar | CharMedusa;

enum CharType {
	Tourtle,
	Shark,
	SeaStar,
	Medusa
}

enum Character {
	Aggressive,
	Pacific
}

enum MoveDirection {
	Stop,
	Up,
	Down,
	Right,
	Left,
	UpRight,
	UpLeft,
	DownRight,
	DownLeft
}

enum DirectionKind {
	Negative,
	Stop,
	Positive
}

class CharEntity {
	speed: number;
	private readonly entity: Phaser.Physics.Arcade.Image;
	definition: CharDefinition;

	constructor(ctx: Phaser.Scene, charDef: CharDefinition, x: number, y: number) {
		this.entity = ctx.physics.add.image(x, y, charDef.name, charDef.textureUrl);
		this.entity.setCollideWorldBounds(true);
		this.definition = charDef;

		this.speed = 300;
	}

	delete() {
		this.entity.destroy(true);
	}

	doMove(directionX: DirectionKind = DirectionKind.Stop, directionY: DirectionKind = DirectionKind.Stop, angle: number = 0, flipX: boolean = false, flipY: boolean = false) {
		const entity = this.entity;

		entity.setVelocityX(this.speed * (directionX - 1));
		entity.setVelocityY(this.speed * (directionY - 1));
		entity.setAngle(angle);
		entity.flipY = flipY;
		entity.flipX = flipX;
	}

	bite() {
		if (this.definition.character === Character.Aggressive) return alert('Кусь');
		alert('Не кусь');
	}

	move(direction: MoveDirection) {
		switch (direction) {
			case MoveDirection.Stop:
				this.entity.setVelocity(0);
				break;

			case MoveDirection.Down:
				this.doMove(DirectionKind.Stop, DirectionKind.Positive, 90, false, true);
				break;

			case MoveDirection.DownLeft:
				this.doMove(DirectionKind.Negative, DirectionKind.Positive, 270 + 45, true, false);
				break;

			case MoveDirection.DownRight:
				this.doMove(DirectionKind.Positive, DirectionKind.Positive, 270 - 45, true, true);
				break;

			case MoveDirection.Left:
				this.doMove(DirectionKind.Negative, DirectionKind.Stop, 180, false, true);
				break;

			case MoveDirection.Right:
				this.doMove(DirectionKind.Positive, DirectionKind.Stop, 0, false, false);
				break;

			case MoveDirection.Up:
				this.doMove(DirectionKind.Stop, DirectionKind.Negative, 270, false, false);
				break;

			case MoveDirection.UpLeft:
				this.doMove(DirectionKind.Negative, DirectionKind.Negative, 90 - 45, true, false);
				break;

			case MoveDirection.UpRight:
				this.doMove(DirectionKind.Positive, DirectionKind.Negative, 90 + 45, true, true);
				break;
		}
	}
}

abstract class CharDefinition {
	declare textureUrl: string;
	declare character: Character;
	declare name: string;
	declare type: CharType;

	protected constructor() { }
}

class CharTourtle extends CharDefinition {
	constructor() {
		super();
		this.name = 'turtle';
		this.textureUrl = 'turtle.svg';
		this.character = Character.Aggressive;
		this.type = CharType.Tourtle;
	}
}

class CharShark extends CharDefinition {
	constructor() {
		super();
		this.name = 'shark';
		this.textureUrl = 'shark.svg';
		this.character = Character.Aggressive;
		this.type = CharType.Shark;
	}
}

class CharSeaStar extends CharDefinition {
	constructor() {
		super();
		this.name = 'seastar';
		this.textureUrl = 'seastar.svg';
		this.character = Character.Pacific;
		this.type = CharType.SeaStar;
	}
}

class CharMedusa extends CharDefinition {
	constructor() {
		super();
		this.name = 'medusa';
		this.textureUrl = 'medusa.svg';
		this.character = Character.Pacific;
		this.type = CharType.Medusa;
	}
}


class Char<T extends CharDefinition> extends CharEntity {
	constructor(ctx: Phaser.Scene, CharDef: new() => T, x: number, y: number) {
		const charDef = new CharDef();
		super(ctx, charDef, x, y);
	}
}

class Game {
	private charDefs: any = {};
	private chars: { [key: string]: Char<Chars> } = {};
	private ctx: Phaser.Scene | null = null;
	private game: Phaser.Game | null = null;
	private cursors: Phaser.Types.Input.Keyboard.CursorKeys | null;
	private io: any;
	private id: string | null = null;
	private currentPlayerType: any;
	private charDefsList: any = [];
	public isStarted: boolean = false;

	constructor() {
		[CharShark, CharTourtle, CharSeaStar, CharMedusa].forEach((row) => {
			const instance = new row();

			this.charDefs[instance.name] = instance;
			this.charDefsList.push(instance);
		});

		this.cursors = null;
		this.ctx = null;
	}

	getCharDefs() {
		return this.charDefs;
	}

	getCharDefsList() {
		return this.charDefsList;
	}

	doUpdate(stateRow: any) {
		console.log('SIO>', stateRow);

		if (!this.chars[stateRow.id]) {
			this.addChar(stateRow.id, stateRow.type);
		}

		this.chars[stateRow.id].move(stateRow.dir);
	}

	start(char: Chars, socket: any) {
		const self = this;
		this.id = socket.id;
		let lastMove: MoveDirection | null = null;

		this.currentPlayerType = char.type;

		function preload(this: any) {
			for (let charDef in self.charDefs) {
				if (!Object.prototype.hasOwnProperty.call(self.charDefs, charDef)) continue;
				const inst = self.charDefs[charDef];
				console.log('/' + inst.textureUrl, inst.name, this.load.image(inst.name, '/' + inst.textureUrl));
			}
		}

		function create(this: Phaser.Scene) {
			self.setCtx(this);
			self.cursors = this.input.keyboard.createCursorKeys();

			const spaceKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
			spaceKey.on('down', () => {
				self.curretPlayer().bite();
			});

			self.isStarted = true;

			self.curretPlayer();
			socket.emit('start');
		}

		function _move(dir: MoveDirection) {
			if (lastMove === dir) return;
			lastMove = dir;

			return socket.emit('updateFromClient', {dir, type: Number(self.currentPlayerType)});
		}

		function update() {
			if (self.id === null || self.cursors === null) return;

			const cursors = self.cursors as Phaser.Types.Input.Keyboard.CursorKeys;
			if (!cursors || !cursors.down || !cursors.left || !cursors.up || !cursors.right) return;

			if (cursors.down.isDown && cursors.right.isDown) {
				return _move(MoveDirection.DownRight);
			}
			if (cursors.down.isDown && cursors.left.isDown) {
				return _move(MoveDirection.DownLeft);
			}
			if (cursors.up.isDown && cursors.right.isDown) {
				return _move(MoveDirection.UpRight);
			}
			if (cursors.up.isDown && cursors.left.isDown) {
				return _move(MoveDirection.UpLeft);
			}
			if (cursors.left.isDown) {
				return _move(MoveDirection.Left);
			}
			if (cursors.right.isDown) {
				return _move(MoveDirection.Right);
			}
			if (cursors.up.isDown) {
				return _move(MoveDirection.Up);
			}
			if (cursors.down.isDown) {
				return _move(MoveDirection.Down);
			}

			return _move(MoveDirection.Stop);
		}

		const config = {
			type           : Phaser.AUTO,
			parent         : 'phaser',
			backgroundColor: '#0072bc',
			physics        : {
				default: 'arcade',
				arcade : {
					debug: false
				}
			},
			scene          : {
				preload,
				create,
				update
			}
		};
		this.game = new Phaser.Game(config);
	}

	curretPlayer(): Char<Chars> {
		const char = this.chars[this.id as string];
		if (char) return char;

		return this.addChar(this.id as string, this.currentPlayerType, 400, 300);
	}

	setCtx(ctx: Phaser.Scene) {
		this.ctx = ctx;
	}

	isReady() {
		return this.ctx !== null;
	}

	private createChar(type: CharType, x: number, y: number): Char<Chars> {
		switch (type) {
			case CharType.Tourtle:
				return new Char<CharTourtle>(this.ctx as Phaser.Scene, CharTourtle, x, y);

			case CharType.Shark:
				return new Char<CharShark>(this.ctx as Phaser.Scene, CharShark, x, y);

			case CharType.SeaStar:
				return new Char<CharSeaStar>(this.ctx as Phaser.Scene, CharSeaStar, x, y);

			case CharType.Medusa:
				return new Char<CharMedusa>(this.ctx as Phaser.Scene, CharMedusa, x, y);
		}
	}

	addChar(id: string, type: CharType, x = 400, y = 300): Char<Chars> {
		return this.chars[id] = this.createChar(type as CharType, x, y);
	}

	deleteChar(id: string) {
		this.chars[id].delete();
		delete this.chars[id];
	}
}

export {Game, CharType};
